package herain.cz.kalkulatoracka.exception;

/**
 * Created by Vít Herain on 2. 8. 2015.
 */
public class CalcResultException extends Exception {

    public CalcResultException(String message) {
        this(message, null);
    }

    public CalcResultException(String message, Throwable cause) {
        super(message, cause);
    }
}
