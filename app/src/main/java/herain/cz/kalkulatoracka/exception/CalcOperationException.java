package herain.cz.kalkulatoracka.exception;

/**
 * Created by V�t Herain on 1. 8. 2015.
 */
public class CalcOperationException extends Exception {

    public CalcOperationException(String message) {
        this(message, null);
    }

    public CalcOperationException(String message, Throwable cause) {
        super(message, cause);
    }
}
