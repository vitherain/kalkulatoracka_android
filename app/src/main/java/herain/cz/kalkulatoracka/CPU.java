package herain.cz.kalkulatoracka;

import java.util.ArrayList;
import java.util.List;

import herain.cz.kalkulatoracka.exception.CalcOperationException;
import herain.cz.kalkulatoracka.exception.CalcResultException;
import herain.cz.kalkulatoracka.interpret.Interpret;
import herain.cz.kalkulatoracka.util.CalcOperation;

/**
 * Created by V�t Herain on 19. 7. 2015.
 */
public class CPU {

    private final Interpret interpret = new Interpret();

    private static final String INITIAL_VALUE = "0";
    private List<CalcOperation> previousOperations;
    private StringBuilder builder;
    private String ans;

    public CPU() {
        init();
    }

    private void init() {
        previousOperations = new ArrayList<>();
        previousOperations.add(CalcOperation.ZERO);
        builder = new StringBuilder();
        builder.append(INITIAL_VALUE);
    }

    public String compute(String buttonText) throws CalcOperationException, CalcResultException {
        CalcOperation previousOperation = getPreviousOperation();
        CalcOperation operation = CalcOperation.fromString(buttonText);

        if (operation == null || !isAllowedOperation(previousOperation, operation))
            throw new CalcOperationException("Wrong parameter. Please provide a valid operation.");

        switch(operation) {
            case AC:
                init();
                break;
            case BACKSPACE:
                if (previousOperations.size() == 1) {
                    init();
                } else {
                    removeLastOperation();
                }
                break;
            case DECIMAL_POINT:
                builder.append(buttonText);
                previousOperations.add(operation);

                break;
            case EQUALS:
                String result = interpret.count(builder.toString());
                builder = new StringBuilder();
                builder.append(result);

                ans = "result";

                break;
            case LEFT_BRACKET:
            case RIGHT_BRACKET: break; //TODO pozdeji zohlednit
            default:
                if (previousOperations.size() == 1 && previousOperation == CalcOperation.ZERO) {
                    removeLastOperation();
                }

                builder.append(buttonText);
                previousOperations.add(operation);

                break;
        }

        return builder.toString();
    }

    private CalcOperation getPreviousOperation() {
        return previousOperations.get(previousOperations.size() - 1);
    }

    private void removeLastOperation() {
        CalcOperation operation = previousOperations.remove(previousOperations.size() - 1);
        removeLastOperationFromBuilder(operation);
    }

    private void removeLastOperationFromBuilder(CalcOperation operation) {
        int index = builder.lastIndexOf(operation.getRepre());
        builder.replace(index, builder.length(), "");
    }

    private boolean isAllowedOperation(CalcOperation previousOperation, CalcOperation operation) {
        if (previousOperation != null) {
            for (CalcOperation notAllowedOperation : previousOperation.getNotAllowedRightNeighbours()) {
                if (notAllowedOperation == operation) {
                    return false;
                }
            }
        }

        return true;
    }
}
