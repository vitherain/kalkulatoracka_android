package herain.cz.kalkulatoracka.util;

/**
 * Created by V�t on 13. 8. 2015.
 */
public class ArrayUtils {

    public static char[] removeElement(char[] array, int index) {
        char[] result = new char[array.length - 1];

        for (int i = 0 ; i < index ; i++) {
            result[i] = array[i];
        }

        for (int i = index + 1 ; i < array.length ; i++) {
            result[i - 1] = array[i];
        }

        return result;
    }
}
