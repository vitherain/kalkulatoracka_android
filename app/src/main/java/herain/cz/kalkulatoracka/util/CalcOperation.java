package herain.cz.kalkulatoracka.util;

/**
 * Created by V�t Herain on 17. 7. 2015.
 */
public enum CalcOperation {

    ZERO("0"),
    ONE("1"),
    TWO("2"),
    THREE("3"),
    FOUR("4"),
    FIVE("5"),
    SIX("6"),
    SEVEN("7"),
    EIGHT("8"),
    NINE("9"),

    PI("PI"),

    DECIMAL_POINT("."),

    ANS("Ans"),

    CHANGE_SIGN("+/-"),

    EQUALS("="),

    PLUS("+"),
    MINUS("-"),
    TIMES("*"),
    DIVIDE("/"),

    LEFT_BRACKET("("),
    RIGHT_BRACKET(")"),

    BACKSPACE("BACKSPACE"),
    AC("AC"),

    SQRT("SQRT");

    static {
        ZERO.notAllowedRightNeighbours = new CalcOperation[]{
                CalcOperation.ANS, CalcOperation.LEFT_BRACKET, CalcOperation.PI, CalcOperation.SQRT
        };
        ONE.notAllowedRightNeighbours = new CalcOperation[]{
                CalcOperation.ANS, CalcOperation.LEFT_BRACKET, CalcOperation.PI, CalcOperation.SQRT
        };
        TWO.notAllowedRightNeighbours = new CalcOperation[]{
                CalcOperation.ANS, CalcOperation.LEFT_BRACKET, CalcOperation.PI, CalcOperation.SQRT
        };
        THREE.notAllowedRightNeighbours = new CalcOperation[]{
                CalcOperation.ANS, CalcOperation.LEFT_BRACKET, CalcOperation.PI, CalcOperation.SQRT
        };
        FOUR.notAllowedRightNeighbours = new CalcOperation[]{
                CalcOperation.ANS, CalcOperation.LEFT_BRACKET, CalcOperation.PI, CalcOperation.SQRT
        };
        FIVE.notAllowedRightNeighbours = new CalcOperation[]{
                CalcOperation.ANS, CalcOperation.LEFT_BRACKET, CalcOperation.PI, CalcOperation.SQRT
        };
        SIX.notAllowedRightNeighbours = new CalcOperation[]{
                CalcOperation.ANS, CalcOperation.LEFT_BRACKET, CalcOperation.PI, CalcOperation.SQRT
        };
        SEVEN.notAllowedRightNeighbours = new CalcOperation[]{
                CalcOperation.ANS, CalcOperation.LEFT_BRACKET, CalcOperation.PI, CalcOperation.SQRT
        };
        EIGHT.notAllowedRightNeighbours = new CalcOperation[]{
                CalcOperation.ANS, CalcOperation.LEFT_BRACKET, CalcOperation.PI, CalcOperation.SQRT
        };
        NINE.notAllowedRightNeighbours = new CalcOperation[]{
                CalcOperation.ANS, CalcOperation.LEFT_BRACKET, CalcOperation.PI, CalcOperation.SQRT
        };

        PI.notAllowedRightNeighbours = new CalcOperation[]{
                CalcOperation.ANS, CalcOperation.LEFT_BRACKET, CalcOperation.PI, CalcOperation.SQRT
        };

        DECIMAL_POINT.notAllowedRightNeighbours =
                new CalcOperation[]{
                        CalcOperation.DECIMAL_POINT, CalcOperation.ANS, CalcOperation.EQUALS,
                        CalcOperation.PLUS, CalcOperation.MINUS, CalcOperation.TIMES, CalcOperation.DIVIDE,
                        CalcOperation.LEFT_BRACKET, CalcOperation.RIGHT_BRACKET, CalcOperation.PI,
                        CalcOperation.SQRT
                };

        ANS.notAllowedRightNeighbours =
                new CalcOperation[]{
                        CalcOperation.ZERO, CalcOperation.ONE, CalcOperation.TWO, CalcOperation.THREE,
                        CalcOperation.FOUR, CalcOperation.FIVE, CalcOperation.SIX, CalcOperation.SEVEN,
                        CalcOperation.EIGHT, CalcOperation.NINE, CalcOperation.PI, CalcOperation.DECIMAL_POINT,
                        CalcOperation.ANS, CalcOperation.SQRT, CalcOperation.LEFT_BRACKET
                };

        CHANGE_SIGN.notAllowedRightNeighbours = new CalcOperation[]{};

        EQUALS.notAllowedRightNeighbours = new CalcOperation[]{};

        PLUS.notAllowedRightNeighbours = new CalcOperation[]{
                CalcOperation.PLUS, CalcOperation.MINUS, CalcOperation.TIMES, CalcOperation.DIVIDE,
                CalcOperation.DECIMAL_POINT, CalcOperation.CHANGE_SIGN, CalcOperation.EQUALS,
                CalcOperation.RIGHT_BRACKET
        };
        MINUS.notAllowedRightNeighbours =
                new CalcOperation[]{
                        CalcOperation.PLUS, CalcOperation.MINUS, CalcOperation.TIMES, CalcOperation.DIVIDE,
                        CalcOperation.DECIMAL_POINT, CalcOperation.CHANGE_SIGN, CalcOperation.EQUALS,
                        CalcOperation.RIGHT_BRACKET
                };
        TIMES.notAllowedRightNeighbours =
                new CalcOperation[]{
                        CalcOperation.PLUS, CalcOperation.MINUS, CalcOperation.TIMES, CalcOperation.DIVIDE,
                        CalcOperation.DECIMAL_POINT, CalcOperation.CHANGE_SIGN, CalcOperation.EQUALS,
                        CalcOperation.RIGHT_BRACKET
                };
        DIVIDE.notAllowedRightNeighbours =
                new CalcOperation[]{
                        CalcOperation.PLUS, CalcOperation.MINUS, CalcOperation.TIMES, CalcOperation.DIVIDE,
                        CalcOperation.DECIMAL_POINT, CalcOperation.CHANGE_SIGN, CalcOperation.EQUALS,
                        CalcOperation.RIGHT_BRACKET
                };

        LEFT_BRACKET.notAllowedRightNeighbours =
                new CalcOperation[]{
                        CalcOperation.DECIMAL_POINT, CalcOperation.TIMES, CalcOperation.DIVIDE,
                        CalcOperation.CHANGE_SIGN, CalcOperation.EQUALS, CalcOperation.RIGHT_BRACKET,
                };
        RIGHT_BRACKET.notAllowedRightNeighbours =
                new CalcOperation[]{
                        CalcOperation.ZERO, CalcOperation.ONE, CalcOperation.TWO, CalcOperation.THREE,
                        CalcOperation.FOUR, CalcOperation.FIVE, CalcOperation.SIX, CalcOperation.SEVEN,
                        CalcOperation.EIGHT, CalcOperation.NINE, CalcOperation.PI, CalcOperation.DECIMAL_POINT,
                        CalcOperation.ANS, CalcOperation.SQRT, CalcOperation.LEFT_BRACKET
                };

        BACKSPACE.notAllowedRightNeighbours = new CalcOperation[]{};
        AC.notAllowedRightNeighbours = new CalcOperation[]{};

        SQRT.notAllowedRightNeighbours =
                new CalcOperation[]{
                        CalcOperation.DECIMAL_POINT, CalcOperation.CHANGE_SIGN, CalcOperation.EQUALS,
                        CalcOperation.TIMES, CalcOperation.DIVIDE, CalcOperation.RIGHT_BRACKET
                };
    }

    private final String repre;
    private CalcOperation[] notAllowedRightNeighbours;

    CalcOperation(String repre) {
        this.repre = repre;
    }

    public String getRepre() {
        return repre;
    }

    public CalcOperation[] getNotAllowedRightNeighbours() {
        return notAllowedRightNeighbours;
    }

    public static CalcOperation fromString(String repre) {
        for (CalcOperation cChar : values()) {
            if (cChar.getRepre().equals(repre))
                return cChar;
        }

        return null;
    }
}
