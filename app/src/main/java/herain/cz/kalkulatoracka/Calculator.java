package herain.cz.kalkulatoracka;

import android.app.Activity;
import android.app.AlertDialog;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.util.logging.Level;
import java.util.logging.Logger;

import herain.cz.kalkulatoracka.exception.CalcOperationException;
import herain.cz.kalkulatoracka.exception.CalcResultException;

public class Calculator extends Activity {

    private final Logger LOGGER = Logger.getLogger("Calculator");

    TextView results;
    CPU cpu = new CPU();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calculator);

        results = (TextView) findViewById(R.id.results);
    }

    public void onButtonClick(View view) {
        Button button = (Button) view;

        try {
            results.setText(cpu.compute(button.getText().toString()));
        } catch (CalcOperationException e) {
            LOGGER.log(Level.INFO, e.getMessage(), e);
            Toast.makeText(this, "Wrong operation", Toast.LENGTH_SHORT).show();
        } catch (CalcResultException e) {
            LOGGER.log(Level.INFO, e.getMessage(), e);
            Toast.makeText(this, "Math error", Toast.LENGTH_SHORT).show();
        }
    }
}
