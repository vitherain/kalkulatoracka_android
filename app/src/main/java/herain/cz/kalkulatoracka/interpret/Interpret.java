package herain.cz.kalkulatoracka.interpret;

import java.util.logging.Level;
import java.util.logging.Logger;

import herain.cz.kalkulatoracka.exception.CalcResultException;
import herain.cz.kalkulatoracka.interpret.util.Parser;

/**
 * Created by Vít Herain on 2. 8. 2015.
 */
public class Interpret {

    private final Logger LOGGER = Logger.getLogger("Interpret");

    private final Parser parser = new Parser();

    public String count(String text) throws CalcResultException {
        try {
            IExpression expression = parser.parse(text);
            return expression.count();
        } catch (Exception e) {
            LOGGER.log(Level.INFO, "There was an error during counting or parsing | " + e.getMessage(), e);
            throw new CalcResultException("There was an error during counting or parsing | " + e.getMessage(), e);
        }
    }
}
