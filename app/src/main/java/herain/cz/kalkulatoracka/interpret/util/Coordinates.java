package herain.cz.kalkulatoracka.interpret.util;

/**
 * Created by Vít Herain on 2. 8. 2015.
 */
public final class Coordinates {

    private final int left, right;

    public Coordinates(int left, int right) {
        this.left = left;
        this.right = right;
    }

    public int getLeft() {
        return left;
    }

    public int getRight() {
        return right;
    }
}
