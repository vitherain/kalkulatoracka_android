package herain.cz.kalkulatoracka.interpret.util;

/**
 * Created by V�t on 3. 8. 2015.
 */
public enum MathOperator {

    PLUS("+"),
    MINUS("-"),
    TIMES("*"),
    DIVIDE("/");

    private final String repre;

    MathOperator(String repre) {
        this.repre = repre;
    }

    public String getRepre() {
        return repre;
    }

    @Override
    public String toString() {
        return getRepre();
    }
}
