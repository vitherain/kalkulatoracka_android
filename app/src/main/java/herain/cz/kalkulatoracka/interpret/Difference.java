package herain.cz.kalkulatoracka.interpret;

import herain.cz.kalkulatoracka.interpret.util.MathOperator;

/**
 * Created by Vít Herain on 2. 8. 2015.
 */
public class Difference implements IExpression {

    private final IExpression first, second;
    private final MathOperator operator = MathOperator.MINUS;

    public Difference(IExpression first, IExpression second) {
        this.first = first;
        this.second = second;
    }

    @Override
    public String count() {
        String firstStr = first.count();
        String secondStr = second.count();

        if (firstStr.contains(".") || secondStr.contains(".")) {
            return floatingCount(firstStr, secondStr);
        }

        return integralCount(Integer.valueOf(firstStr), Integer.valueOf(secondStr));
    }

    private String integralCount(int first, int second) {
        return String.valueOf(first - second);
    }

    private String floatingCount(String first, String second) {
        int maxDecimals = getMaxDecimals(first, second);
        long corrector = buildCorrector(maxDecimals);

        double firstD = Double.valueOf(first) * corrector;
        double secondD = Double.valueOf(second) * corrector;

        return String.valueOf((firstD - secondD) / corrector);
    }

    private int getMaxDecimals(String first, String second) {
        int firstDecimalIndex = first.indexOf('.');
        int secondDecimalIndex = second.indexOf('.');

        if (firstDecimalIndex != -1) {
            firstDecimalIndex = first.length() - 1 - firstDecimalIndex;
        }

        if (secondDecimalIndex != -1) {
            secondDecimalIndex = second.length() - 1 - secondDecimalIndex;
        }

        return firstDecimalIndex > secondDecimalIndex ? firstDecimalIndex : secondDecimalIndex;
    }

    private long buildCorrector(int maxDecimals) {
        StringBuilder builder = new StringBuilder();
        builder.append("1");

        for (int i = 1 ; i <= maxDecimals ; i++) {
            builder.append("0");
        }

        return Long.valueOf(builder.toString());
    }

    @Override
    public String toString() {
        return first.count() + "-" + second.count();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Difference that = (Difference) o;

        if (first != null ? !first.equals(that.first) : that.first != null) return false;
        if (second != null ? !second.equals(that.second) : that.second != null) return false;
        return operator == that.operator;

    }

    @Override
    public int hashCode() {
        int result = first != null ? first.hashCode() : 0;
        result = 31 * result + (second != null ? second.hashCode() : 0);
        result = 31 * result + (operator != null ? operator.hashCode() : 0);
        return result;
    }
}
