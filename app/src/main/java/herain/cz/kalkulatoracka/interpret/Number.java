package herain.cz.kalkulatoracka.interpret;

/**
 * Created by V�t Herain on 1. 8. 2015.
 */
public class Number implements IExpression {

    private final String number;

    public Number(String number) {
        this.number = number;
    }

    @Override
    public String count() {
        return number;
    }

    @Override
    public String toString() {
        return number;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Number number1 = (Number) o;

        return !(number != null ? !number.equals(number1.number) : number1.number != null);
    }

    @Override
    public int hashCode() {
        return number != null ? number.hashCode() : 0;
    }
}
