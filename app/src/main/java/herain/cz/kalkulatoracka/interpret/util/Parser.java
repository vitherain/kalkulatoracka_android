package herain.cz.kalkulatoracka.interpret.util;

//import org.apache.commons.lang3.ArrayUtils;

import java.util.logging.Level;
import java.util.logging.Logger;

import herain.cz.kalkulatoracka.exception.CalcResultException;
import herain.cz.kalkulatoracka.interpret.*;
import herain.cz.kalkulatoracka.interpret.Number;
import herain.cz.kalkulatoracka.util.ArrayUtils;

/**
 * Created by Vít Herain on 2. 8. 2015.
 */
public class Parser {

    private final Logger LOGGER = Logger.getLogger("Parser");

    public IExpression parse(String source) throws CalcResultException {
        checkNumberOfBrackets(source);

        char[] sourceArray = source.toCharArray();
        int lastLeftBracketIndex = -1;

        for (int i = 0 ; i < sourceArray.length ; i++) {
            if (sourceArray[i] == '(') lastLeftBracketIndex = i;

            if (sourceArray[i] == ')') {
                if (lastLeftBracketIndex >= 0) {
                    IExpression expressionInBrackets = parseSimpleExpression(source.substring(lastLeftBracketIndex + 1, i));
                    String result = expressionInBrackets.count();
                    String newSource = buildNewSource(source, result, lastLeftBracketIndex, i);

                    return parse(newSource);
                } else {
                    LOGGER.log(Level.INFO, "There was an error during parsing: unexpected character ')'");
                    throw new CalcResultException("There was an error during parsing: unexpected character ')'");
                }
            }
        }

        return parseSimpleExpression(source);
    }

    private String buildNewSource(String source, String result, int lastLeftBracketIndex, int i) {
        String leftPart = source.substring(0, lastLeftBracketIndex);
        String rightPart = source.substring(i + 1, source.length());

        if (result.startsWith("-")) {
            int rightBracketsCount = 0;
            int leftBracketsCount = 0;
            char[] leftArray = leftPart.toCharArray();

            for (int j = leftArray.length - 1 ; j >= 0 ; j--) {
                if (leftArray[j] == ')') rightBracketsCount++;
                else if (leftArray[j] == '(') leftBracketsCount++;
                else if (leftArray[j] == '+' && (leftBracketsCount == rightBracketsCount || leftBracketsCount == rightBracketsCount + 1)) {
                    leftArray[j] = '-';
                    result = result.replace("-", "");
                    break;
                }
                else if (leftArray[j] == '-' && (leftBracketsCount == rightBracketsCount || leftBracketsCount == rightBracketsCount + 1)) {
                    leftArray[j] = '+';
                    result = result.replace("-", "");
                    break;
                }
            }

            leftPart = new String(leftArray);
        }

        return leftPart + result + rightPart;
    }

    private boolean isInBrackets() {

    }

    private void checkNumberOfBrackets(String source) throws CalcResultException {
        int leftBracketsCount = 0;
        int rightBracketsCount = 0;

        char[] sourceArray = source.toCharArray();

        for (int i = 0 ; i < source.length() ; i++) {
            if (sourceArray[i] == '(') leftBracketsCount++;
            if (sourceArray[i] == ')') rightBracketsCount++;
        }

        if (leftBracketsCount != rightBracketsCount) throw new CalcResultException("Wrong brackets count");
    }

    protected IExpression parseSimpleExpression(String source) {
        String[] cleny = source.split("\\" + MathOperator.PLUS.getRepre());

        if (cleny.length > 1 && !(cleny.length == 2 && cleny[0].equals(""))) {
            return new Sum(parseSimpleExpression(getRestOfArrayConcat(cleny, MathOperator.PLUS)), parseSimpleExpression(cleny[cleny.length - 1]));
        }

        cleny = source.split(MathOperator.MINUS.getRepre());

        if (cleny.length > 1 && !(cleny.length == 2 && cleny[0].equals(""))) {
            return new Difference(parseSimpleExpression(getRestOfArrayConcat(cleny, MathOperator.MINUS)), parseSimpleExpression(cleny[cleny.length - 1]));
        }

        cleny = source.split("\\" + MathOperator.TIMES.getRepre());

        if (cleny.length > 1) {
            return new Product(parseSimpleExpression(getRestOfArrayConcat(cleny, MathOperator.TIMES)), parseSimpleExpression(cleny[cleny.length - 1]));
        }

        cleny = source.split("\\" + MathOperator.DIVIDE.getRepre());

        if (cleny.length > 1) {
            return new Quotient(parseSimpleExpression(getRestOfArrayConcat(cleny, MathOperator.DIVIDE)), parseSimpleExpression(cleny[cleny.length - 1]));
        }

        return cleny[0].startsWith("+") ? new Number(cleny[0].substring(1)) : new Number(cleny[0]);
    }

    private String getRestOfArrayConcat(String[] cleny, MathOperator operator) {
        StringBuilder builder = new StringBuilder();

        //posledni index se vynechává
        for (int i = 0 ; i < cleny.length - 1 ; i++) {
            builder.append(cleny[i]);

            if (i < cleny.length - 2)
                builder.append(operator.getRepre());
        }

        return builder.toString();
    }
}
