package herain.cz.kalkulatoracka.interpret;

import herain.cz.kalkulatoracka.interpret.util.MathOperator;

/**
 * Created by Vít Herain on 2. 8. 2015.
 */
public class Quotient implements IExpression {

    private static final double RESULT_CORRECTOR = 1000 * 1000 * 1000 * 1.0;
    private final IExpression first, second;
    private final MathOperator operator = MathOperator.DIVIDE;

    public Quotient(IExpression first, IExpression second) {
        this.first = first;
        this.second = second;
    }

    @Override
    public String count() {
        String firstStr = first.count();
        String secondStr = second.count();

        return cleanDecimals(floatingCount(firstStr, secondStr));
    }

    private String cleanDecimals(String number) {
        if (number.contains(".")) {
            String decimalPart = number.substring(number.indexOf(".") + 1, number.length());

            return areOnlyZeros(decimalPart) ? number.substring(0, number.indexOf(".")) : number;
        }

        return number;
    }

    private boolean areOnlyZeros(String number) {
        for (char ch : number.toCharArray()) {
            if (ch != '0')
                return false;
        }

        return true;
    }

    private String floatingCount(String first, String second) {
        int maxDecimals = getMaxDecimals(first, second);
        long corrector = buildCorrector(maxDecimals);

        double firstD = Double.valueOf(first) * corrector;
        double secondD = Double.valueOf(second) * corrector;

        double resultD = firstD / secondD;

        return String.valueOf(Math.round(resultD * RESULT_CORRECTOR) / RESULT_CORRECTOR);
    }

    private int getMaxDecimals(String first, String second) {
        int firstDecimalIndex = first.indexOf('.');
        int secondDecimalIndex = second.indexOf('.');

        if (firstDecimalIndex != -1) {
            firstDecimalIndex = first.length() - 1 - firstDecimalIndex;
        }

        if (secondDecimalIndex != -1) {
            secondDecimalIndex = second.length() - 1 - secondDecimalIndex;
        }

        return firstDecimalIndex > secondDecimalIndex ? firstDecimalIndex : secondDecimalIndex;
    }

    private long buildCorrector(int maxDecimals) {
        StringBuilder builder = new StringBuilder();
        builder.append("1");

        for (int i = 1 ; i <= maxDecimals ; i++) {
            builder.append("0");
        }

        return Long.valueOf(builder.toString());
    }

    @Override
    public String toString() {
        return first.count() + "/" + second.count();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Quotient quotient = (Quotient) o;

        if (first != null ? !first.equals(quotient.first) : quotient.first != null) return false;
        if (second != null ? !second.equals(quotient.second) : quotient.second != null)
            return false;
        return operator == quotient.operator;
    }

    @Override
    public int hashCode() {
        int result = first != null ? first.hashCode() : 0;
        result = 31 * result + (second != null ? second.hashCode() : 0);
        result = 31 * result + (operator != null ? operator.hashCode() : 0);
        return result;
    }
}
