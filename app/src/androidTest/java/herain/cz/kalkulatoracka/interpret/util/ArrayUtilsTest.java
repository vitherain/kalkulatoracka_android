package herain.cz.kalkulatoracka.interpret.util;

import android.test.InstrumentationTestCase;

import herain.cz.kalkulatoracka.util.ArrayUtils;

/**
 * Created by V�t on 13. 8. 2015.
 */
public class ArrayUtilsTest extends InstrumentationTestCase {

    public void testRemoveElement() {
        char[] arrayToRemoveFrom = new char[] {
                'a', 'b', 'c'
        };

        arrayToRemoveFrom = ArrayUtils.removeElement(arrayToRemoveFrom, 0);

        assertTrue(arrayToRemoveFrom[0] != 'a');
        assertTrue(arrayToRemoveFrom[0] == 'b');
        assertTrue(arrayToRemoveFrom[1] == 'c');
        assertTrue(arrayToRemoveFrom.length == 2);

        arrayToRemoveFrom = new char[] {
                'a', 'b', 'c'
        };

        arrayToRemoveFrom = ArrayUtils.removeElement(arrayToRemoveFrom, 1);

        assertTrue(arrayToRemoveFrom[1] != 'b');
        assertTrue(arrayToRemoveFrom[0] == 'a');
        assertTrue(arrayToRemoveFrom[1] == 'c');
        assertTrue(arrayToRemoveFrom.length == 2);

        arrayToRemoveFrom = new char[] {
                'a', 'b', 'c', 'd'
        };

        arrayToRemoveFrom = ArrayUtils.removeElement(arrayToRemoveFrom, 2);

        assertTrue(arrayToRemoveFrom[2] != 'c');
        assertTrue(arrayToRemoveFrom[0] == 'a');
        assertTrue(arrayToRemoveFrom[1] == 'b');
        assertTrue(arrayToRemoveFrom[2] == 'd');
        assertTrue(arrayToRemoveFrom.length == 3);
    }
}
