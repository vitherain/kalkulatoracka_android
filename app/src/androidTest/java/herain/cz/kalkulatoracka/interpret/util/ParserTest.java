package herain.cz.kalkulatoracka.interpret.util;

import android.test.InstrumentationTestCase;

import herain.cz.kalkulatoracka.interpret.*;
import herain.cz.kalkulatoracka.interpret.Number;

/**
 * Created by V�t on 5. 8. 2015.
 */
public class ParserTest extends InstrumentationTestCase {

    private Parser parser = new Parser();

    public void testParseSimpleExpressionPlus() {
        String source = "5+6";
        IExpression result = parser.parseSimpleExpression(source);
        IExpression expected = new Sum(new Number("5"), new Number("6"));

        assertTrue(expected.equals(result));

        source = "5.1+6.001+7.5";
        result = parser.parseSimpleExpression(source);
        expected = new Sum(
                new Sum(
                        new Number("5.1"),
                        new Number("6.001")
                ),
                new Number("7.5")

        );

        assertTrue(expected.equals(result));

        source = "35.000052+2.00006";
        result = parser.parseSimpleExpression(source);
        expected = new Sum(new Number("35.000052"), new Number("2.00006"));

        assertTrue(expected.equals(result));

        source = "+1+2";
        result = parser.parseSimpleExpression(source);
        expected = new Sum(new Number("1"), new Number("2"));

        assertTrue(expected.equals(result));
    }

    public void testParseSimpleExpressionMinus() {
        String source = "5-6";
        IExpression result = parser.parseSimpleExpression(source);
        IExpression expected = new Difference(new Number("5"), new Number("6"));

        assertTrue(expected.equals(result));

        source = "5.5-6.0001";
        result = parser.parseSimpleExpression(source);
        expected = new Difference(new Number("5.5"), new Number("6.0001"));

        assertTrue(expected.equals(result));

        source = "5-6-4";
        result = parser.parseSimpleExpression(source);
        expected = new Difference(

                new Difference(
                        new Number("5"),
                        new Number("6")
                ),
                new Number("4")
        );

        assertTrue(expected.equals(result));

        source = "-1-2";
        result = parser.parseSimpleExpression(source);
        expected = new Difference(new Number("-1"), new Number("2"));

        assertTrue(expected.equals(result));
    }

    public void testParseSimpleExpressionTimes() {
        String source = "5*6";
        IExpression result = parser.parseSimpleExpression(source);
        IExpression expected = new Product(new Number("5"), new Number("6"));

        assertTrue(expected.equals(result));

        source = "5*5*6";
        result = parser.parseSimpleExpression(source);
        expected = new Product(
                new Product(
                        new Number("5"),
                        new Number("5")
                ),
                new Number("6")
        );

        assertTrue(expected.equals(result));

        source = "5.002*6.00025";
        result = parser.parseSimpleExpression(source);
        expected = new Product(new Number("5.002"), new Number("6.00025"));

        assertTrue(expected.equals(result));
    }

    public void testParseSimpleExpressionDivide() {
        String source = "30/6";
        IExpression result = parser.parseSimpleExpression(source);
        IExpression expected = new Quotient(
                new Number("30"),
                new Number("6")
        );

        assertTrue(expected.equals(result));

        source = "30/6/2";
        result = parser.parseSimpleExpression(source);
        expected = new Quotient(
                new Quotient(
                        new Number("30"),
                        new Number("6")
                ),
                new Number("2")
        );

        assertTrue(expected.equals(result));

        source = "30.555526/2.50000";
        result = parser.parseSimpleExpression(source);
        expected = new Quotient(
                new Number("30.555526"),
                new Number("2.50000")
        );

        assertTrue(expected.equals(result));
    }

    public void testParseSimpleExpressionCombination() {
        String source = "30/6+12-9/3*5";
        IExpression result = parser.parseSimpleExpression(source);
        IExpression expected = new Sum(
                new Quotient(
                        new Number("30"),
                        new Number("6")
                ),
                new Difference(
                        new Number("12"),
                        new Product(
                                new Quotient(
                                        new Number("9"),
                                        new Number("3")
                                ),
                                new Number("5")
                        )
                )
        );

        assertTrue(expected.equals(result));
    }

    public void testParseSimpleExpressionSignFirst() {
        String source = "-2";
        IExpression result = parser.parseSimpleExpression(source);
        IExpression expected = new Number("-2");

        assertTrue(expected.equals(result));

        source = "-2-1";
        result = parser.parseSimpleExpression(source);
        expected = new Difference(
                new Number("-2"),
                new Number("1")
        );

        assertTrue(expected.equals(result));

        source = "+2+1";
        result = parser.parseSimpleExpression(source);
        expected = new Sum(
                new Number("2"),
                new Number("1")
        );

        assertTrue(expected.equals(result));

        source = "+2";
        result = parser.parseSimpleExpression(source);
        expected = new Number("2");

        assertTrue(expected.equals(result));
    }
}
