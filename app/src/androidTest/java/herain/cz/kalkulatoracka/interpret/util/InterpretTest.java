package herain.cz.kalkulatoracka.interpret.util;

import android.test.InstrumentationTestCase;

import herain.cz.kalkulatoracka.exception.CalcResultException;
import herain.cz.kalkulatoracka.interpret.Interpret;

/**
 * Created by V�t on 10. 8. 2015.
 */
public class InterpretTest extends InstrumentationTestCase {

    private Interpret interpret = new Interpret();
    private Parser parser = new Parser();

    public void testCountPlus() throws CalcResultException {
        String source = "5+6";
        String result = interpret.count(source);
        String expected = "11";

        assertTrue(expected.equals(result));

        source = "5.1+6.001+7.5";
        result = interpret.count(source);
        expected = "18.601";

        assertTrue(expected.equals(result));

        source = "35.000052+2.00006";
        result = interpret.count(source);
        expected = "37.000112";

        assertTrue(expected.equals(result));

        source = "+1.2";
        result = interpret.count(source);
        expected = "1.2";

        assertTrue(expected.equals(result));

        source = "+1+2";
        result = interpret.count(source);
        expected = "3";

        assertTrue(expected.equals(result));
    }

    public void testCountMinus() throws CalcResultException {
        String source = "5-6";
        String result = interpret.count(source);
        String expected = "-1";

        assertTrue(expected.equals(result));

        source = "5.5-6.0001";
        result = interpret.count(source);
        expected = "-0.5001";

        assertTrue(expected.equals(result));

        source = "-2.5555";
        result = interpret.count(source);
        expected = "-2.5555";

        assertTrue(expected.equals(result));

        source = "-1-2";
        result = interpret.count(source);
        expected = "-3";

        assertTrue(expected.equals(result));
    }

    public void testCountTimes() throws CalcResultException {
        String source = "5*6";
        String result = interpret.count(source);
        String expected = "30";

        assertTrue(expected.equals(result));

        source = "5*5*6";
        result = interpret.count(source);
        expected = "150";

        assertTrue(expected.equals(result));

        source = "5.002*6.00025";
        result = interpret.count(source);
        expected = "30.0132505";

        assertTrue(expected.equals(result));
    }

    public void testCountDivide() throws CalcResultException {
        String source = "30/6";
        String result = interpret.count(source);
        String expected = "5";

        assertTrue(expected.equals(result));

        source = "30/6/2";
        result = interpret.count(source);
        expected = "2.5";

        assertTrue(expected.equals(result));

        source = "30.555526/2.50000";
        result = interpret.count(source);
        expected = "12.2222104";

        assertTrue(expected.equals(result));
    }

    public void testCountDividePeriodic() throws CalcResultException {
        String source = "10/3";
        String result = interpret.count(source);
        String expected = "3.333333333";

        assertTrue(expected.equals(result));
    }

    public void testCountCombination() throws CalcResultException {
        String source = "30/6+12-9/3*5";
        String result = interpret.count(source);
        String expected = "2";

        assertTrue(expected.equals(result));

        source = "30/6+12-8/3*5";
        result = interpret.count(source);
        expected = "3.666666665";

        assertTrue(expected.equals(result));
    }

    public void testCountSignFirst() throws CalcResultException {
        String source = "-2";
        String result = interpret.count(source);
        String expected = "-2";

        assertTrue(expected.equals(result));

        source = "-2-1";
        result = interpret.count(source);
        expected = "-3";

        assertTrue(expected.equals(result));

        source = "+2+1";
        result = interpret.count(source);
        expected = "3";

        assertTrue(expected.equals(result));

        source = "+2";
        result = interpret.count(source);
        expected = "2";

        assertTrue(expected.equals(result));
    }

    public void testCountComplex() throws CalcResultException {
        String source = "((5+12)*3-(13-14)*3/2)";
        String result = interpret.count(source);
        String expected = "52.5";

        assertTrue(expected.equals(result));

        source = "(-((5+12)*3*(13-14)*3/2))";
        result = interpret.count(source);
        expected = "76.5";

        assertTrue(expected.equals(result));

        source = "(-(((5+12)*3*(13-14)*3/2)))";
        result = interpret.count(source);
        expected = "76.5";

        assertTrue(expected.equals(result));
    }

    public void testWrongResults() throws CalcResultException {
        String source;
        String result;
        String expected;

        try {
            source = "*2";
            result = interpret.count(source);

            fail( "Did not throw expected exception" );
        } catch (CalcResultException exc) {
        }

        try {
            source = "/2";
            result = interpret.count(source);

            fail( "Did not throw expected exception" );
        } catch (CalcResultException exc) {
        }

        try {
            source = "2+*3";
            result = interpret.count(source);

            fail( "Did not throw expected exception" );
        } catch (CalcResultException exc) {
        }
    }
}
